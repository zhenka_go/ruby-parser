require 'open-uri'
require 'nokogiri'
require 'csv'

url = 'http://www.slush.org/why-attend/startups/'
html = open(url)

doc = Nokogiri::HTML(html)
startups = []
doc.css('.startups-listing__item').each do |startup|
  a = startup.css('h5 a')
  title = a.text
  url = a.attr('href').value
  categories = []
  startup.css('.startups-listing__item__tags li').each do |tag|
    categories.push(tag.text)
  end

  startups.push(
      title: title,
      url: url,
      categories: categories
  )
end

CSV.open('someshit.csv', 'wb') do |file|
  file << ['title', 'url', 'categories']

  startups.each do |s|
    file << [ s[:title], s[:url], s[:categories] ]
  end
end
